<?php namespace App\Controllers;

use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use App\Helpers\RoutingHelper;

class Calender
{

    protected $client;
    public $response = [];

    const CREDENTIALS_PATH = '../configs/credentials.json';

    /**
     * Calender constructor.
     * @throws \Google_Exception
     */
    public function __construct()
    {
        $client = new Google_Client();
        $client->setAuthConfig(self::CREDENTIALS_PATH);
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;

    }

    public function index()
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $calendarId = 'primary';
            $results = $service->events->listEvents($calendarId);
            $this->response['items'] = $results->getItems();

        } else {
            RoutingHelper::go('oauth');
        }
    }

    public function create()
    {

    }

    /**
     * @param $request
     * @throws \Exception
     */
    public function store($request)
    {

        session_start();
        $startDateTime = (new \DateTime($request['start_date']))->format(\DateTime::ISO8601);
        $endDateTime = (new \DateTime($request['end_date']))->format(\DateTime::ISO8601);
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $calendarId = 'primary';
            $event = new Google_Service_Calendar_Event([
                'summary' => $request['summary'],
                'description' => $request['description'],
                'start' => ['dateTime' => $startDateTime],
                'end' => ['dateTime' => $endDateTime],
                'reminders' => ['useDefault' => true],
            ]);
            $results = $service->events->insert($calendarId, $event);
            if (!$results) {
                $this->response = ['status' => 'error', 'message' => 'Something went wrong'];
            }
            $this->response = ['status' => 'success', 'message' => 'Event Created'];
        } else {
            RoutingHelper::go('oauth');
        }
    }



    public function oauth()
    {
        session_start();
        $routeUrl = RoutingHelper::getUrl('oauth', true);
        $this->client->setRedirectUri($routeUrl);
        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
            RoutingHelper::redirect($filtered_url);
        } else {
            $this->client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->client->getAccessToken();
            RoutingHelper::go('index');
        }

    }
}
