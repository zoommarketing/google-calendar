<?php

use App\Controllers\Calender;

try {
    $calender = new Calender();
} catch (Exception $exception)
{
    echo $exception->getMessage();
}


switch ($page = ($_GET['page'])??null) {

    case 'create':
        $calender->create();
        require_once '../public/theme/create.php';
        break;
    case 'store':
        $calender->store($_POST);
        $page = $calender->response;
        require_once '../public/theme/store.php';
        break;
    case 'oauth':
        $calender->oauth();
        break;
    default:
        $calender->index();
        $page = $calender->response;
        require_once '../public/theme/index.php';

}
