<?php namespace App\Helpers;


class RoutingHelper
{

    const FULL_PATH = 'http://localhost/calender/public/';

    public static function getUrl($page = 'index', $fullPath = false) {
        return (($fullPath)?self::FULL_PATH:'').'./?page='.$page;
    }

    public static function go($page) {
        self::redirect(self::getUrl($page));
    }

    public static function redirect($url) {
        ob_start();
        header('Location: '.$url);
        ob_end_flush();
        die();
    }
}
