<?php require_once 'header.php'; ?>

<h1>Create</h1>
<form method="post" action="<?=\App\Helpers\RoutingHelper::getUrl('store')?>">
    <input type="text" name="summary" placeholder="Tytuł">
    <input type="text" name="description" placeholder="Opis">
    <input type="datetime-local" name="start_date">
    <input type="datetime-local" name="end_date">
    <input type="submit">
</form>
