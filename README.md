# GOOGLE CALENDAR

## Instalacja

W wierszu poleceń wpisujemy

```sh
git clone https://zoommarketing@bitbucket.org/zoommarketing/google-calendar.git
```

a następnie
 
```sh
composer install
```

## Tutorial

Na stronie 

```
https://developers.google.com/calendar/quickstart/php
```

dostępna jest dokumentacja. 

| Methoda | Opis |
| ------ | ------ |
|configs/credentials.json| Plik wygenerowany na stronie podanej wyżej.
|public/theme| Pliki HTML

Cała logika jest zawarta w
 
```
App\Controllers\Calender
```

| Methoda | Opis |
| ------ | ------ |
|construct| Tutaj jest dostępna logowanie.
|index| Pobieranie eventów dla domyślnego kalendarza
|create| Formularz z tworzeniem Eventu
|store| Zapisywanie Eventu
|oauth| Odbieranie kodu autoryzacji





